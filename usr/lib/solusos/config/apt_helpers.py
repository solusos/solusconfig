import apt
import apt.progress.base
import apt_pkg
import os
import logging
import fcntl

class CustomAcquireMonitor(apt.progress.base.AcquireProgress):

   def __init__(self, callback):
	apt.progress.base.AcquireProgress.__init__(self)
	self.callback = callback

   def fetch (self, item):
	pct = 100 *  float (self.current_bytes) / float (self.total_bytes)
	self.callback ("download", pct)

   def pulse (self, owner):
	pct = 100 *  float (self.current_bytes) / float (self.total_bytes)
	self.callback ("download", pct)

class CustomInstallMonitor(apt.progress.base.InstallProgress):

   def __init__(self, callback, remove=False):
	apt.progress.base.InstallProgress.__init__(self)
	self.callback = callback
	self.removing = remove

	#self.status_parent_fd, self.status_child_fd = os.pipe ()

        (self.statusfd, self.writefd) = os.pipe()
        self.write_stream = os.fdopen(self.writefd, "w")
        self.status_stream = os.fdopen(self.statusfd, "r")
        fcntl.fcntl(self.statusfd, fcntl.F_SETFL, os.O_NONBLOCK)


   def status_change (self, pkg, pct, status):
	our_pkg = str(pkg)
	our_pct = float (pct)
	if not self.removing:
		self.callback ("install", our_pct)
	else:
		self.callback (our_pct)


   def fork (self):
        pid, self.master_fd = os.forkpty()
        if pid == 0:
		os.putenv("DEBIAN_FRONTEND", "noninteractive")
		os.putenv("APT_LISTCHANGES_FRONTEND", "none")

	return pid

   def run(self, obj):
        """Install using the object 'obj'.

        This functions runs install actions. The parameter 'obj' may either
        be a PackageManager object in which case its do_install() method is
        called or the path to a deb file.

        If the object is a PackageManager, the functions returns the result
        of calling its do_install() method. Otherwise, the function returns
        the exit status of dpkg. In both cases, 0 means that there were no
        problems.
        """
        pid = self.fork()
        if pid == 0:
            # pm.do_install might raise a exception,
            # when this happens, we need to catch
            # it, otherwise os._exit() is not run
            # and the execution continues in the
            # parent code leading to very confusing bugs
            try:
                os._exit(obj.do_install(self.write_stream.fileno()))
            except AttributeError:
                os._exit(os.spawnlp(os.P_WAIT, "dpkg", "dpkg", "--status-fd",
                                    str(self.write_stream.fileno()), "-i",
                                    obj))
            except Exception, e:
		logging.debug ("Exception: %s" % e.message)
                os._exit(apt_pkg.PackageManager.RESULT_FAILED)

        self.child_pid = pid
        res = self.wait_child()
        return os.WEXITSTATUS(res)


