#!/usr/bin/env python
import gobject
import dbus
import dbus.service
import dbus.glib
import shutil

from polkit_helper import PolkitHelper
import detection
from detection import HardwareID

# Privileges
from privs_misc import *

import subprocess
import sys
import threading
import re
import logging
import os.path

import apt
import shutil

from apt_helpers import CustomAcquireMonitor, CustomInstallMonitor

class ConfigService(dbus.service.Object):

    CONFIGURE_AUDIO   = "com.solusos.configuration.audio"
    CONFIGURE_DRIVERS = "com.solusos.configuration.driver"

    def __init__(self, loop):
        bus_name = dbus.service.BusName('com.solusos.config', bus = dbus.SystemBus())
        dbus.service.Object.__init__(self, bus_name, '/com/solusos/Configuration')

	self.dbus_info = None

	self.polkit = PolkitHelper()

	self.pid_map = dict()
	self.loop = loop

	# Weird as it may sound this is a dict of lists.
	self.action_pids = dict()

	self.modaliases = detection.get_modaliases ()
	self._package_operation_in_progress = False

	# Drop privileges immediately. We don't need root access unless we're doing something
	drop_privileges()


    def _is_pulse_fixed(self):
	tsched_found = False
	with open("/etc/pulse/default.pa", "r") as pulseconfig:
		for line in pulseconfig.readlines():
			line = line.replace("\r","").replace("\n","")
			if "tsched" in line and "module-udev-detect" in line:
				tsched_found = True
				break
	return tsched_found


    def _set_pulse_fixed(self,fixed):
	if self._is_pulse_fixed() and fixed:
		return
	if not self._is_pulse_fixed() and not fixed:
		return

	with open("/etc/pulse/default.pa", "r") as pulseconfig:
		regain_privileges()
		with open("/etc/pulse/TMPCONFIG", "w") as newconfig:
			for line in pulseconfig.readlines():
				line = line.replace("\r","").replace("\n","")
				if "load-module module-udev-detect" in line:
					if fixed:
						newconfig.write("load-module module-udev-detect tsched=0\n")
					else:
						newconfig.write("load-module module-udev-detect\n")
				else:
					newconfig.write("%s\n" % line)
		drop_privileges()

	# Move the files
	try:
		regain_privileges()
		shutil.move("/etc/pulse/default.pa", "/etc/pulse/default.pa.BAK")
		shutil.move("/etc/pulse/TMPCONFIG", "/etc/pulse/default.pa")
		shutil.copystat("/etc/pulse/default.pa.BAK", "/etc/pulse/default.pa")
		drop_privileges()
	except Exception, ex:
		print ex

	print "Fixing PulseAudio"

    ''' Return the process ID for the specified connection '''
    def get_pid_from_connection(self, conn, sender):
        if self.dbus_info is None:
            self.dbus_info = dbus.Interface(conn.get_object('org.freedesktop.DBus',
                '/org/freedesktop/DBus/Bus', False), 'org.freedesktop.DBus')
        pid = self.dbus_info.GetConnectionUnixProcessID(sender)

	return pid


    ''' Very much around the houses. Monitor connection and hold onto process id's.
	This way we know who is already "authenticated" and who is not '''
    def register_connection_with_action(self, conn, sender, action_id):
	pid = self.get_pid_from_connection(conn, sender)

	if sender not in self.pid_map:
		print "Adding new sender: %s" % sender
		self.pid_map[sender] = pid

	if action_id not in self.action_pids:
		self.action_pids[action_id] = list()

	def cb(conn,sender):
		# Complicated, doesn't really need a lambda but in the future for whatever reason
		# we may need the sender and connection objects
		if conn == "":
			pid = None
			try:
				pid = self.pid_map[sender]
			except:
				# already removed, called twice.
				return
			print "Disconnected process: %d" % pid

			self.pid_map.pop(sender)
			count = 0
			for i in self.action_pids[action_id]:
				if i == pid:
					self.action_pids[action_id].pop(count)
					break
				count += 1
			del count
	conn.watch_name_owner(sender, lambda x: cb(x, sender))


    ''' Is PulseAudio already fixed ? '''
    @dbus.service.method("com.solusos.config", sender_keyword="sender", connection_keyword="conn", out_signature="b")
    def GetPulseAudioFixed(self, sender=None, conn=None):
	# Being nice we allow anyone to see the status of whether pulseaudio is fixed or not.
	fixed = self._is_pulse_fixed()
	return fixed


    ''' Apply the PulseAudio fix '''
    @dbus.service.method("com.solusos.config", sender_keyword="sender", connection_keyword="conn", in_signature="b")
    def SetPulseAudioFixed(self, fixed, sender=None, conn=None):


	# I know this could be a simple return statement but this needs to be a readable example :p
	# Soon I'll be adding actual work-code here.
	if self.persist_authorized(sender, conn, self.CONFIGURE_AUDIO):
		self._set_pulse_fixed(fixed)
		return True
	else:
		return False

    @dbus.service.method("com.solusos.config", sender_keyword="sender", connection_keyword="conn", out_signature="b")
    def IsNvidiaSupported(self, sender=None, conn=None):
	''' Quite simply, will the nvidia proprietary driver work on this box ? '''
	with open ("/usr/lib/solusos/config/modaliases/nvidia.aliases", "r") as nv_aliases:
		for line in nv_aliases.readlines ():
			line = line.replace("\r","").replace("\n","")

			for alias in self.modaliases:
				if alias == HardwareID('modalias', line):
					return True
	return False

    @dbus.service.method("com.solusos.config", sender_keyword="sender", connection_keyword="conn", out_signature="b")
    def IsAmdSupported(self, sender=None, conn=None):
	''' Quite simply, will the amd proprietary driver work on this box ? '''
	with open ("/usr/lib/solusos/config/modaliases/fglrx.aliases", "r") as f_aliases:
		for line in f_aliases.readlines ():
			line = line.replace("\r","").replace("\n","")

			for alias in self.modaliases:
				if alias == HardwareID('modalias', line):
					return True
	return False


    ''' Utility Method. Check if the sender is authorized to perform this action. If so, add them to a persistent list.
	List is persistent until this object is destroyed. Works via process ID's '''
    def persist_authorized(self, sender,conn, action_id):
	self.register_connection_with_action(conn,sender,action_id)

	pid = self.pid_map[sender]

	if not pid in self.action_pids[action_id]:
		if self.polkit.check_authorization(pid, action_id):
			self.action_pids[action_id].append(pid)
			return True # Authorized by PolKit!

		else:
			return False # Unauthorized by PolKit!
	else:
		return True # Already authorized by PolKit in this session


    ''' Shut down this service '''
    @dbus.service.method('com.solusos.config',  sender_keyword='sender',  connection_keyword='conn')
    def ShutDown(self, sender=None, conn=None):
	# No special checks required as of yet, but in future, flag quit for after finishing

	print "Shutdown requested"

	# you can't just do a sys.exit(), this causes errors for clients
	self.loop.quit()

    def install_package(self, package, progress_cb):
	regain_privileges ()
	package_group = []
	if package == "nvidia":
		package_group = ["nvidia-glx", "nvidia-kernel-dkms", "glx-alternative-nvidia", "nvidia-settings", "nvidia-xconfig", "vdpau-va-driver"]
	elif package == "fglrx":
		package_group = ["fglrx-driver", "fglrx-glx", "fglrx-control", "fglrx-modules-dkms", "glx-alternative-fglrx"]

	mon_fetch = CustomAcquireMonitor (progress_cb)
	mon_install = CustomInstallMonitor (progress_cb)

	cache = apt.Cache ()
	for pkg in package_group:
		cache[pkg].mark_install ()
	try:
		cache.commit (fetch_progress=mon_fetch, install_progress=mon_install)
	except SystemError, e:
		self.driver_install_error (e.message)
		drop_privileges ()
		return False
	except Exception:
		pass

	ret = -1
	# Should make this a lickle bit moar better
	if package == "nvidia":
		# -1 = not yet configured
		#  0 = configured ok
		#  2 = failed to configure
		self.install_progress ("configure", ret)
		os.system ("nvidia-xconfig")
		ret = 0 if os.path.exists ("/etc/X11/xorg.conf") else 2
		self.install_progress ("configure", ret)
	elif package == "fglrx":
		self.install_progress ("configure", ret)
		os.system ("aticonfig --initial -f")
		ret = 0 if os.path.exists ("/etc/X11/xorg.conf") else 2
		self.install_progress ("configure", ret)
	self.driver_install_complete ()
	drop_privileges ()

	return True

            
    def remove_package(self, package, progress_cb):
	regain_privileges ()
	package_group = []
	if package == "nvidia":
		package_group = ["nvidia-glx", "nvidia-kernel-dkms", "glx-alternative-nvidia", "nvidia-settings", "nvidia-xconfig", "vdpau-va-driver", "nvidia-vdpau-driver", "nvidia-kernel-common"]
	elif package == "fglrx":
		package_group = ["fglrx-driver", "fglrx-glx", "fglrx-control", "fglrx-modules-dkms", "glx-alternative-fglrx"]

	mon_fetch = CustomAcquireMonitor (progress_cb)
	mon_install = CustomInstallMonitor (progress_cb,remove=True)

	cache = apt.Cache ()
	for pkg in package_group:
		cache[pkg].mark_delete (purge=True)
	try:
		cache.commit (fetch_progress=mon_fetch, install_progress=mon_install)
	except SystemError, e:
		self.driver_install_error (e.message)
		drop_privileges ()
		return False
	except Exception:
		pass

	# Move the X11 config
	if os.path.exists ("/etc/X11/xorg.conf"):
		shutil.move ("/etc/X11/xorg.conf", "/etc/X11/xorg.conf.SOLUSBACKUP")

	self.driver_install_complete ()
	drop_privileges ()

    def _reset_timeout(self):
        '''Reset the D-BUS server timeout.'''

        self._timeout = False

    @dbus.service.method("com.solusos.config",
        in_signature='sb', out_signature='b', sender_keyword='sender',
        connection_keyword='conn')
    def SetDriverEnabled(self, package_name, enable, sender=None, conn=None):
        '''Enable or disable a driver.

        This enables (enable=True) or disables (enable=False) the given Driver
        ID. Return True if the handler could be activated immediately, or False
        if the system needs a reboot for the changes to become effective.
        '''
        self._reset_timeout()
        if not self.persist_authorized (sender, conn, self.CONFIGURE_DRIVERS):
		return False
        if enable:
            f = lambda x : self.install_package (x, self.install_progress)
        else:
            f = lambda x : self.remove_package (x, self.remove_progress)

        if not conn:
            # not called through D-BUS, thus don't send progress signals
            return f(package_name)

	# start progress information early; for package operations we will
	# always have a progress bar, avoid delays from the package manager
	# with progress reporting
	if enable:
		self.install_progress('download', -1)
	else:
		self.remove_progress(-1)

        def _f_result_wrapper():
            try:
                self._f_result = f(package_name)
            except:
                self._f_exception = sys.exc_info()

        # Call enable/disable in a separate thread, in case it does long
        # actions and thus we need to signal progress
        self._f_exception = None
        t_f = threading.Thread(None, _f_result_wrapper,
            'thread_enable_disable', [], {})
        t_f.start()
        while True:
            t_f.join(0.2)
            if not t_f.isAlive():
                break
            self._reset_timeout ()
            if enable:
                    self.install_progress('install', -1)
            else:
                    self.remove_progress(-1)
        if self._f_exception:
            raise self._f_exception[0], self._f_exception[1], self._f_exception[2]

        return self._f_result

    @dbus.service.signal("com.solusos.config")
    def install_progress(self, phase, curr):
        '''Report package installation progress.

        'phase' is 'download' or 'install'. current and/or total might be -1 if
        time cannot be determined.
        '''
        return False # TODO: cancel not implemented

    @dbus.service.signal("com.solusos.config")
    def remove_progress(self, curr):
        '''Report package removal progress.

        current and/or total might be -1 if time cannot be determined.
        '''
        return False

    @dbus.service.signal("com.solusos.config")
    def driver_install_error(self, message):
	''' Report a problem, always requires exit '''
	return False

    @dbus.service.signal("com.solusos.config")
    def driver_install_complete (self):
	''' Driver installation complete! '''
	return False

